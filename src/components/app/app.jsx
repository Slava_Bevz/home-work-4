import React from 'react';
import './app.css';
import Header from '../header/header.jsx';
import Main from '../main/main.jsx'
import Footer from '../footer/footer.jsx'; 


export default function App () {
    return (
        <div className='body'>
            <Header></Header>
            <Main></Main>
            <Footer></Footer>
        </div>
    )
}
