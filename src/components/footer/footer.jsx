import React from 'react';
import './footer.css';

const Footer = () => {
    return (
        <div className='footer'>
            <div className='footer__box wrapper'>
                <div className='footer__info'>
                    <h3 className='footer__title'>САМЫЕ УМНЫЕ ПРОЕКТЫ</h3>
                    <p className='footer__text'>РЕАЛИЗУЕМ САМЫЕ СМЕЛЫЕ РЕШЕНИЯ</p>
                </div>
                <button className='button' type='button'>
                    <img src='/img/envelope.svg'/>
                    <p className='button__text'>Ваш запрос</p>
                </button>
            </div>
        </div>
    )
}

export default Footer;