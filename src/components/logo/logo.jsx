import React from 'react';
import './logo.css';

export default function Logo () {
    return (
        <header className='logo-menu'>
            <a href='#'>
                <img src='./img/logo.svg' alt='logo'></img>
            </a>
            <a href="#">
                <img src='./img/menu-gamburger.svg' alt="menu"></img>
            </a>
        </header>
    )
}