import React from 'react';
import './years.css';

export default function Years ({year}) {

    return (
        <div className="years__box">
            <span className="years__years">{year}</span>
            <span className="years__text">лет</span>
            <span className="years__market">на рынке</span>
        </div>
    )
}
