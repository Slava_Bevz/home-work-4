import React from 'react';
import '../product/product.css';

const Product = ({ name, text, url }) => {
    return (
        <div className='product'>
            <div className='product__box-img'>
                <img  className='product__img' src={url} alt={name}></img>
            </div>
            <span className='product__line'></span>
            <span className='product__name'>{name}</span>
            <p className='product__text'>{text}</p>
        </div>
    )
}

export default Product;