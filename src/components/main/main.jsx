import React from 'react';
import './main.css';
import { project } from '../../data/data';
import Product from '../product/product.jsx'

const Main = () => {
    return (
        <div className='wrapper'>
            <div className='main'>
                <h3 className='main__title'>Наши самые большие проекты</h3>
                <div className='product-box'>
                    {
                        project.map((e) => <Product name={e.name} text={e.text} url={e.url}/>)
                    }
                </div>
            </div>
        </div>
    )
}

export default Main;