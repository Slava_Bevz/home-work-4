import React from 'react';
import './header.css';
import Logo from '../logo/logo.jsx';
import Title from '../title/title.jsx';
import { market } from '../../data/data.js';
import Years from '../years/years.jsx';


const Header = () => {
    return (
        <div className="wallpaper">
            <div className="wrapper">
                <Logo></Logo>
                <Title></Title>
                <div className='years'>
                    {
                        market.map( (e) => <Years year={e.year}></Years> )
                    }
                </div>
            </div>   
        </div> 
    )
};

export default Header;