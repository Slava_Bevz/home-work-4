import React from 'react';
import './title.css';

export default function Title () {
    return (
        <div className='banner'>
            <h2 className='banner__title'>Реализуем крупнейшие проекты в Украине</h2>
            <p className='banner__text'>стадионы, газопроводы, мосты, дамбы</p>
        </div>
    )
}