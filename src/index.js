import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/app/app.jsx';
import './style.css';

ReactDOM.render(<App></App>, document.querySelector('#root'));